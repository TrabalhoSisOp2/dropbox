#include "dropboxUtil.h"

/**
 * Abre o diretório dirname
 * Caso o mesmo não exista, será criado
 * Diretório ja existente, retorna 1, recém criado, retorna 2
 **/
int open_directory(char * dirname)
{
	DIR* dir = opendir(dirname);

	if (dir)
	{
		closedir(dir);
		return DIR_ALREADY_EXISTS;
	}
	else if (ENOENT == errno)
	{
		if (mkdir(dirname, 0700) == 0)
			return DIR_JUST_CREATED;
	}

	return DIR_FAILED;
}


/**
 * Obtem a listagem dos arquivos de um diretório, cada arquivo separado por '\n'
 **/
char * get_file_list(char * path)
{
	DIR *dp;
	struct dirent *ep;
	char * output, * buf, * buf2;
	int i;

	dp = opendir(path);

	if (dp)
	{
		i = 0;
		output = NULL;

		while ((ep = readdir(dp)))
		{
			if (strcmp(ep->d_name, ".") == 0)
				continue;

			if (strcmp(ep->d_name, "..") == 0)
				continue;

			if (ep->d_name[0] == '.')
				continue;

			if (i == 0)
			{
				output = calloc(strlen(ep->d_name) + 1, sizeof(char));
				strcpy(output, ep->d_name);
			}
			else
			{
				buf = calloc(strlen(ep->d_name) + 1, sizeof(char));
				strcpy(buf, ep->d_name);

				buf2 = calloc(strlen(output) + strlen(buf) + 2, sizeof(char));
				sprintf(buf2, "%s\n%s", output, buf);

				//free(buf);
				//free(output);
				output = buf2;
			}
			i++;
		}

		closedir(dp);

		return output;
	}

	return NULL;
}


/**
 * Obtem a listagem dos arquivos de um diretório,
 * aloca e retorna a string correspondente
 * às informações do diretório ou NULL caso haja algum erro
 **/
char * list_directory(char * path)
{
	DIR *dp;
	struct dirent *ep;
	char * output, * buf, * buf2;
	char filename[BUFFER_SIZE];

	dp = opendir(path);

	if (dp)
	{
		output = calloc(BUFFER_SIZE, sizeof(char));

		if (output)
		{
			sprintf(output,"%-24s%-10s%-22s%-22s%-22s",
				"Name",
				"Size",
				"Creation",
				"Access",
				"Modification"
			);

			while ((ep = readdir(dp)))
			{
				if (strcmp(ep->d_name, ".") == 0)
					continue;

				if (strcmp(ep->d_name, "..") == 0)
					continue;

				if (ep->d_name[0] == '.')
					continue;

				sprintf(filename, "%s/%s", path, ep->d_name);
				buf = list_file(filename);

				if (buf)
				{
					buf2 = calloc(strlen(buf) + strlen(output) + 2, sizeof(char));

					if (buf2)
					{
						sprintf(buf2, "%s\n%s", output, buf);
						free(output);
						output = buf2;
					}

					free(buf);
				}
			}

			closedir(dp);

			return output;
		}
	}

	return NULL;
}


/**
 * Informações sobre um arquivo, aloca e retorna a string correspondente
 * às informações do arquivo ou NULL caso haja algum erro
 **/
char * list_file(char * path)
{
	struct stat sb;
	char * name;
	char buffer[BUFFER_SIZE];
	char fctime[TIME_SIZE], fatime[TIME_SIZE], fmtime[TIME_SIZE];

	if (stat(path, &sb) != -1)
	{
		name = basename(path);
		strftime(fctime, TIME_SIZE, "%Y-%m-%d %H:%M:%S", localtime(&sb.st_ctime));
		strftime(fatime, TIME_SIZE, "%Y-%m-%d %H:%M:%S", localtime(&sb.st_atime));
		strftime(fmtime, TIME_SIZE, "%Y-%m-%d %H:%M:%S", localtime(&sb.st_mtime));

		sprintf(buffer,"%-24s%-10lld%-22s%-22s%-22s",
			name,
			(long long) sb.st_size,
			fctime,
			fatime,
			fmtime
		);

		return strdup(buffer);
	}

	return NULL;
}


/**
 * Escreve o dado 'data' com 'size' bytes no socket ssl
 * Retorna 1 em caso de sucesso e 0 em caso de falha
 **/
static int send_data_sub(SSL * ssl, char * data, int size)
{
	int sent, bytes;

	sent = 0;

	do
	{
		bytes = SSL_write(ssl, data + sent, size - sent);

		if (bytes < 0)
		{
			return 0;
		}

		sent += bytes;
	}
	while(sent < size);

	return 1;
}


/**
 * Usado para enviar o dado 'data' com 'size' bytes através do socket
 * protegido por ssl
 * Retorna 1 em caso de sucesso e 0 em caso de falha
 **/
int send_data(SSL * ssl, char * data, int size)
{
	char buf[256];

	/* Enviando quantidade de dados a ser transferida */
	sprintf(buf, "%-255d", size);

	if (send_data_sub(ssl, buf, 256) == 0)
		return 0;

	/* Enviando os dados */
	if (send_data_sub(ssl, data, size) == 0)
		return 0;

	return 1;
}


/**
 * Lê 'size' bytes do socket protegido por ssl
 * Retorna o conteudo lido ou NULL em caso de falha
 **/
static char * read_data_sub(SSL * ssl, int size)
{
	char * data;
	int readed, bytes;

	data = calloc(size, sizeof(char));
	readed = 0;

	if (data)
	{
		do
		{
			bytes = SSL_read(ssl, data + readed, size - readed);

			if (bytes < 0)
			{
				free(data);
				return NULL;
			}

			readed += bytes;
		}
		while(readed < size);

		return data;
	}

	return NULL;
}


/**
 * Usado para ler dados do socket protegido por ssl
 * O tamanho da leitura é retornado por referência em 'size'
 * Retorna o conteudo lido ou NULL em caso de falha
 **/
 char * read_data(SSL * ssl, int * size)
 {
	char * data;

	/* Lendo quantidade de dados a ser transferida */
	data = read_data_sub(ssl, 256);

	if (data == NULL)
	{
		*size = 0;
		return NULL;
	}

	*size = atoi(data);
	free(data);

	/* Lendo os dados */
	data = read_data_sub(ssl, *size);

	if (data == NULL)
	{
		*size = 0;
		return NULL;
	}

	return data;
 }


/**
 * Faz a leitura de um arquivo para a memória e retorna
 * O tamanho do arquivo é salvo por referência em size
 **/
char * read_file(char * path, int * size)
{
	FILE * f;
	char * output = NULL;

	f = fopen(path, "rb");

	if (f)
	{
		fseek(f, 0, SEEK_END);
		*size = ftell(f);
		fseek(f, 0, SEEK_SET);

		output = calloc(*size, sizeof(char));

		if (output != NULL)
		{
			fread(output, *size, 1, f);
		}

		fclose(f);
	}

	return output;
}


/**
 * Escreve em um arquivo. Caso o arquivo não exista, o mesmo é criado
 **/
void write_file(char * path, char * data, int size)
{
	FILE *f;

	if ((f = fopen(path, "w")))
	{
		if (size > 0)
		{
			fwrite(data, size, 1, f);
		}

		fclose(f);
	}
}


/**
 * Obtém o caminho para a pasta home do usuário
 **/
char * get_home()
{
	char * home;

	if ((home = getenv("HOME")) == NULL)
	{
		return (home = getpwuid(getuid())->pw_dir);
	}

	return home;
}


/**
 * Retorna o modification time de um arquivo
 **/
time_t get_mtime(char * path)
{
	struct stat sb;

	if (stat(path, &sb) != -1)
	{
		return sb.st_mtime;
	}

	return DEFAULT_TIME;
}


/**
 * Define o modification time de um arquivo
 **/
int set_mtime(char * path, time_t t)
{
	struct stat sb;
	struct utimbuf new_times;

	if (stat(path, &sb) != -1)
	{
		new_times.actime = sb.st_atime;
		new_times.modtime = t;

		if (utime(path, &new_times) == 0)
		{
			return 1;
		}
	}

	return 0;
}


/**
 * Apaga um arquivo do disco
 **/
int delete_file(char *file)
{
	if (remove(file) == 0)
	{
		return 1;
	}

	return 0;
}


/**
 * Verifica se um arquivo existe no disco
 **/
int file_exist(char *filename)
{
	struct stat buffer;
	return (stat(filename, &buffer) == 0);
}


/**
 * Conta ocorrências de 'search' em 'string'
 **/
int substr_count(char * search, char * string)
{
	int count = 0;
	const char *tmp;

	if (search && string)
	{
		tmp = string;

		while((tmp = strstr(tmp, search)))
		{
			count++;
			tmp++;
		}
	}

	return count;
}


/**
 * Quebra 'src' delimitado por 'tokens' em um array e retorna-o por referência
 * em 'list'. O tamanho é retornado por referência em 'len'
 * Retorna NULL caso não exista 'tokens' em 'src'
 **/
void explode(const char *src, const char *tokens, char ***list, int *len)
{
	char *str, *copy, **_list = NULL, **tmp, *save;

	*list = NULL;
	*len  = 0;

	if (src == NULL || list == NULL || len == NULL)
		return;

	copy = strdup(src);

	if (copy == NULL)
		return;

	str = strtok_r(copy, tokens, &save);

	if (str == NULL)
		goto free_and_exit;

	_list = realloc(NULL, sizeof *_list);

	if (_list == NULL)
		goto free_and_exit;

	_list[*len] = strdup(str);

	if (_list[*len] == NULL)
		goto free_and_exit;

	(*len)++;

	while((str = strtok_r(NULL, tokens, &save)))
	{
		tmp = realloc(_list, (sizeof *_list) * (*len + 1));

		if (tmp == NULL)
			goto free_and_exit;

		_list = tmp;
		_list[*len] = strdup(str);

		if (_list[*len] == NULL)
			goto free_and_exit;

		(*len)++;
	}

	free_and_exit:
		*list = _list;
		free(copy);
}
