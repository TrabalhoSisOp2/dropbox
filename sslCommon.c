#include "sslCommon.h"

void ShowCerts(SSL* ssl)
{
	X509 *cert;
	char *line;

	cert = SSL_get_peer_certificate(ssl); /* Get certificates (if available) */

	if ( cert != NULL )
	{
		printf("Certificates:\n");
		line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
		printf("Subject: %s\n", line);
		free(line);
		line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
		printf("Issuer: %s\n", line);
		free(line);
		X509_free(cert);
	}
	else
	{
		printf("No certificates.\n");
	}
}

void CloseSSL(SSL * ssl)
{
	if (ssl)
	{
		SSL_shutdown(ssl);
		SSL_free(ssl);
	}
}