CC=gcc

all: clean client server

client: dropboxClient.o dropboxUtil.o lista.o sslCommon.o sslClient.o sslServer.o frontEnd.o
	$(CC) -g -o client dropboxClient.o dropboxUtil.o lista.o sslCommon.o  sslClient.o sslServer.o frontEnd.o -Wall -lpthread -lssl -lcrypto

server: dropboxServer.o dropboxUtil.o lista.o sslCommon.o sslServer.o
	$(CC) -g -o server dropboxServer.o dropboxUtil.o lista.o sslCommon.o sslServer.o -Wall -lpthread -lssl -lcrypto

dropboxClient.o: dropboxClient.c dropboxClient.h
	$(CC) -g -c dropboxClient.c -Wall

dropboxServer.o: dropboxServer.c dropboxServer.h
	$(CC) -g -c dropboxServer.c -Wall

dropboxUtil.o: dropboxUtil.c dropboxUtil.h
	$(CC) -g -c dropboxUtil.c -Wall

lista.o: lista.c lista.h
	$(CC) -g -c lista.c -Wall

sslCommon.o: sslCommon.c sslCommon.h
	$(CC) -g -c sslCommon.c -Wall -lssl -lcrypto

sslClient.o: sslClient.c sslClient.h
	$(CC) -g -c sslClient.c -Wall -lssl -lcrypto

sslServer.o: sslServer.c sslServer.h
	$(CC) -g -c sslServer.c -Wall -lssl -lcrypto

frontEnd.o: frontEnd.c frontEnd.h
	$(CC) -g -c frontEnd.c -Wall -lssl -lcrypto

clean:
	rm -rf client server *.o  *~ *.dSYM
