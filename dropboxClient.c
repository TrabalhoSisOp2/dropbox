#include "dropboxUtil.h"
#include "dropboxClient.h"
#include "sslCommon.h"
#include "sslClient.h"
#include "frontEnd.h"

SSL * client_ssl;
int client_fd;
char userid[MAXNAME];
char homedir[BUFFER_SIZE];
char systemdir[BUFFER_SIZE];
char server_ip[BUFFER_SIZE];
int server_port;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

char* optionslist[] = {"upload", "download", "list_server", "list_client",
						"get_sync_dir", "get_file_list", "exit", "remove_client",
						"remove_server", "request_mtime", "change_mtime"};

/// New (Heartbeat)
int isalive = 1;
int heart_fd;
struct sockaddr_in serv_addr_heartbeat;

fd_set readfds, masterfds;
struct timeval timeout;

int count_alive = 0;
/// End New (Heartbeat)

/**
 * Conecta o cliente com o servidor.
 * host - endere�o do servidor
 * port - port aguardando conex�o
 * Retorna o fd da conex�o
 **/
int connect_server(char * host, int port)
{
	int size;
	char * buffer;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	SSL_CTX *ctx;

	SSL_library_init();
	ctx = InitCTX();

	server = gethostbyname(host);

	if (server == NULL)
	{
		 printf("No such host\n");
	}
	else
	{
		if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		{
			printf("Error opening socket\n");
		}
		else
		{
			serv_addr.sin_family = AF_INET;
			serv_addr.sin_port = htons(port);
			serv_addr.sin_addr = *((struct in_addr *)server->h_addr);
			bzero(&(serv_addr.sin_zero), 8);

			if (connect(client_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
			{
				printf("Error connecting\n");
			}
			else
			{

				client_ssl = SSL_new(ctx);
				SSL_set_fd(client_ssl, client_fd);

				if (SSL_connect(client_ssl) == -1)
				{
					ERR_print_errors_fp(stderr);
					exit(1);
				}

				if (send_data(client_ssl, userid, strlen(userid) + 1))
				{
					buffer = read_data(client_ssl, &size);

					if (buffer)
					{
						printf("%s\n", buffer);

						if (strstr(buffer, "Welcome"))
						{
							free(buffer);

							return 1;
						}

						free(buffer);
					}
					else
					{
						printf("Error getting response\n");
					}
				}
				else
				{
					printf("Error sending data\n");
				}
			}
		}
	}

	return 0;
}

/**
 *
 **/
void option_selector(int optioncode, char* token)
{
	switch(optioncode)
	{
		case MENU_OPTION_UPLOAD:
		{
			token = strtok(NULL, "\n");

			if (token)
			{
				upload_command(token);
			}
			else
			{
				printf("File name not set\n");
			}
		}
		break;

		case MENU_OPTION_DOWNLOAD:
		{
			token = strtok(NULL, "\n");

			if (token)
			{
				download_command(token);
			}
			else
			{
				printf("File name not set\n");
			}
		}
		break;

		case MENU_OPTION_LISTSERVER:
		{
			list_server_command();
		}
		break;

		case MENU_OPTION_LISTCLIENT:
		{
			list_client_command(homedir);
		}
		break;

		case MENU_OPTION_GETFILELIST:
		{
			char * buf;

			buf = get_file_list_command();

			if (buf)
			{
				printf("%s\n", buf);
				free(buf);
			}
		}
		break;

		case MENU_OPTION_GETSYNCDIR:
		{
 			get_sync_dir_command();
 		}
		break;

		case MENU_OPTION_EXIT:
		{
		}
		break;

		case MENU_OPTION_REMOVE_SERVER:
		{
			token = strtok(NULL, "\n");

			if (token)
			{
				remove_command(token);
			}
			else
			{
				printf("File name not set\n");
			}
		}
		break;

		case MENU_OPTION_GET_MTIME:
		{
			time_t t;

			token = strtok(NULL, "\n");

			if (token)
			{
				t = request_mtime_command(token);
				printf("%ld\n", t);
			}
			else
			{
				printf("File name not set\n");
			}
		}
		break;

		case MENU_OPTION_CHANGE_MTIME:
		{
			time_t t;

			token = strtok(NULL, " ");

			if (token)
			{
				t = atoi(token);
				token = strtok(NULL, "\n");

				if (token)
				{
					change_mtime_command(token, t);
				}
			}
			else
			{
				printf("File name not set\n");
			}
		}
		break;

		default:
		{
			help_command();
		}
	}
}

/**
 *
 **/
void client_commands()
{
	char * token;
	char buffer[BUFFER_SIZE];
	char command[BUFFER_SIZE];
	int optioncode;
	int i;

	do
	{
		bzero(buffer, BUFFER_SIZE);
		bzero(command, BUFFER_SIZE);

		printf("command> ");
		fgets(buffer, BUFFER_SIZE, stdin);
		buffer[strcspn(buffer, "\r\n")] = 0;

		memmove(command, buffer, strlen(buffer) + 1);
		token = strtok(command, " ");

		if (token == NULL)
			continue;

		int stringsize = strlen(command);

		for (i = 0; i < stringsize; i++)
		{
			command[i] = tolower(command[i]);
		}

		optioncode = MENU_OPTION_INVALID;

		for (i = 0; i < MENU_OPTIONS_AMOUNT; i++)
		{
			if (strcmp(command, optionslist[i]) == 0)
			{
				optioncode = i;
			}
		}

		LOCK
		option_selector(optioncode, token);
		UNLOCK
	}
	while (optioncode != MENU_OPTION_EXIT);
}

/**
 *
 **/
int upload_command(char * path)
{
	char * buffer, output[BUFFER_SIZE];
	int size, result;
	time_t mtime;

	if (!file_exist(path))
	{
		return 0;
	}

	result = 0;
	sprintf(output, "upload %s", basename(path));

	if (send_data(client_ssl, output, strlen(output) + 1))
	{
		buffer = read_file(path, &size);

		if (buffer)
		{
			if (send_data(client_ssl, buffer, size))
			{
				free(buffer);
				buffer = read_data(client_ssl, &size);

				if (buffer)
				{
					if (strcmp(buffer, "error") != 0)
					{
						mtime = get_mtime(path);
						change_mtime_command(path, mtime);
						result = 1;
					}

					free(buffer);
				}
			}
			else
			{
				free(buffer);
			}
		}
	}

	return result;
}

/**
 *
 **/
int download_command(char * filename)
{
	int size, result;
	char * buffer, output[BUFFER_SIZE];
	time_t mtime;

	result = 0;
	sprintf(output, "download %s", basename(filename));

	if (send_data(client_ssl, output, strlen(output) + 1))
	{
		buffer = read_data(client_ssl, &size);

		if (buffer)
		{
			if (strcmp(buffer, "error") != 0)
			{
				write_file(filename, buffer, size);
				mtime = request_mtime_command(filename);
				set_mtime(filename, mtime);
				result = 1;
			}

			free(buffer);
		}
	}

	return result;
}

/**
 *
 **/
int list_server_command()
{
	int size, result;
	char * buffer, output[BUFFER_SIZE];

	result = 0;
	sprintf(output, "list_server");

	if (send_data(client_ssl, output, strlen(output) + 1))
	{
		buffer = read_data(client_ssl, &size);

		if (buffer)
		{
			if (strcmp(buffer, "error") != 0)
			{
				printf("%s\n", buffer);
				result = 1;
			}

			free(buffer);
		}
	}

	return result;
}

/**
 *
 **/
int list_client_command(char * path)
{
	char * buffer;
	int result;

	result = 0;
	buffer = list_directory(path);

	if (buffer)
	{
		printf("%s\n", buffer);
		result = 1;
		free(buffer);
	}

	return result;
}

/**
 *
 **/
char * get_file_list_command()
{
	int size;
	char * buffer, * result, output[BUFFER_SIZE];

	result = NULL;
	sprintf(output, "get_file_list");

	if (send_data(client_ssl, output, strlen(output) + 1))
	{
		buffer = read_data(client_ssl, &size);

		if (buffer)
		{
			if (strcmp(buffer, "error") != 0)
			{
				result = buffer;
			}
		}
	}

	return result;
}

/**
 *
 **/
int remove_command(char * filename)
{
	char * buffer, output[BUFFER_SIZE];
	int size, result;

	result = 0;
	sprintf(output, "delete_file %s", basename(filename));

	if (send_data(client_ssl, output, strlen(output) + 1))
	{
		buffer = read_data(client_ssl, &size);

		if (buffer)
		{
			if (strcmp(buffer, "error") != 0)
			{
				result = 1;
			}

			free(buffer);
		}
	}

	return result;
}

/**
 *
 **/
time_t request_mtime_command(char * filename)
{
	int size;
	char output[BUFFER_SIZE], * buffer;
	time_t result;

	result = DEFAULT_TIME;
	sprintf(output, "%s %s", "request_mtime", basename(filename));

	if (send_data(client_ssl, output, strlen(output) + 1))
	{
		buffer = read_data(client_ssl, &size);

		if (buffer)
		{
			if (strcmp(buffer, "error") != 0)
			{
				result = atoi(buffer);
			}

			free(buffer);
		}
	}

	return result;
}

/**
 *
 **/
int change_mtime_command(char * filename, time_t t)
{
	char * buffer, output[BUFFER_SIZE];
	int size, result;

	result = 0;
	sprintf(output, "%s %ld %s", "change_mtime", t, basename(filename));

	if (send_data(client_ssl, output, strlen(output) + 1))
	{
		buffer = read_data(client_ssl, &size);

		if (buffer)
		{
			if (strcmp(buffer, "error") != 0)
			{
				result = 1;
			}

			free(buffer);
		}
	}

	return result;
}


/**
 *
 **/
void help_command()
{
	printf("\n Opcoes disponiveis:\n\n");
	printf("  upload <filename> - envia um arquivo identificado por <filename>.\n");
	printf("  download <filename> - baixa um arquivo identificado por <filename>.\n");
	printf("  list_server - lista os arquivos armazenados remotamente, no servidor.\n");
	printf("  list_client - lista os arquivos armazenados localmente.\n");
	printf("  get_sync_dir - sincroniza o diret�rio local com o diret�rio remoto.\n");
	printf("  exit - sai do sistema.\n\n");
}

/**
 *
 **/
int exit_command()
{
	char * buffer, output[BUFFER_SIZE];
	int size, result;

	result = 0;
	sprintf(output, "exit");

	if (send_data(client_ssl, output, strlen(output) + 1))
	{
		buffer = read_data(client_ssl, &size);

		if (buffer)
		{
			if (strcmp(buffer, "error") != 0)
			{
				result = 1;
			}

			free(buffer);
		}
	}

	return result;
}

/**
 *
 **/
void get_sync_dir_command()
{
	sync_dir();
}

/**
 * Captura sinais
 **/
void sig_handler(int signo)
{
	if (signo == SIGINT)
	{
		printf("received SIGINT\n");

		LOCK
		exit_command();
		CloseSSL(client_ssl);
		close(client_fd);
		UNLOCK

		exit(0);
	}
}


/**
 * Inicializa diret�rio do usu�rio e diret�rio do sistema
 **/
void init_dir()
{
	int open;

	open = open_directory(homedir);

	switch(open)
	{
		case DIR_FAILED:
			printf("Failed to create sync dir\n");
			exit(1);
	}

	open = open_directory(systemdir);

	switch(open)
	{
		case DIR_FAILED:
			printf("Failed to create system dir\n");
			exit(1);
	}
}


/**
 * Faz a sincroniza��o no sentido cliente->servidor
 *
 * Testa 4 condi��es:
 *
 *		(1) - F e FS existem e s�o diferentes
 *				Modificado, upload de F e atualiza FS
 *		(2) - F e FS existem e s�o iguais
 *				Igual, faz nada
 *		(3)	- F existe e FS n�o
 *				Novo, faz upload de F e cria FS
 *		(4) - FS existe e F n�o
 *				Apagado, remove_server F e apaga FS
 * Obs:
 *	F	-> Arquivo no sync dir do usu�rio
 *	FS	-> Arquivo de controle do arquivo correspondente no sync dir
 *
 **/
void sync_client_to_server()
{
	char * files, * files_sys;
	char ** files_arr, ** files_sys_arr;
	int files_size, files_sys_size;

	int i, j, found;
	time_t mtime, mtime_sys;
	char path[BUFFER_SIZE], path_sys[BUFFER_SIZE];

	files_size = 0;
	files_sys_size = 0;

	files = get_file_list(homedir);
	files_sys = get_file_list(systemdir);

	if (files)
	{
		explode(files, "\n", &files_arr, &files_size);
		free(files);
	}

	if (files_sys)
	{
		explode(files_sys, "\n", &files_sys_arr, &files_sys_size);
		free(files_sys);
	}

	for (i = 0; i < files_size; i++)
	{
		found = 0;
		sprintf(path, "%s/%s", homedir, files_arr[i]);

		for (j = 0; j < files_sys_size; j++)
		{
			if (strcmp(files_arr[i], files_sys_arr[j]) == 0)
			{
				found = 1;
				sprintf(path_sys, "%s/%s", systemdir, files_sys_arr[j]);
				break;
			}
		}

		if (found == 1)
		{
			mtime = get_mtime(path);
			mtime_sys = get_mtime(path_sys);

			if (difftime(mtime, mtime_sys) != 0)
			{
				upload_command(path);
				set_mtime(path_sys, mtime);
			}
		}
		else
		{
			upload_command(path);
			sprintf(path_sys, "%s/%s", systemdir, files_arr[i]);
			write_file(path_sys, "", 0);
			set_mtime(path_sys, mtime);
		}
	}

	for (i = 0; i < files_sys_size; i++)
	{
		found = 0;
		sprintf(path_sys, "%s/%s", systemdir, files_sys_arr[i]);

		for (j = 0; j < files_size; j++)
		{
			if (strcmp(files_sys_arr[i], files_arr[j]) == 0)
			{
				found = 1;
				break;
			}
		}

		if (found == 0)
		{
			delete_file(path_sys);
			remove_command(path_sys);
		}
	}

	if (files_size > 0)
	{
		for (i = 0; i < files_size; i++)
			free(files_arr[i]);
		free(files_arr);
	}

	if (files_sys_size > 0)
	{
		for (i = 0; i < files_sys_size; i++)
			free(files_sys_arr[i]);
		free(files_sys_arr);
	}
}


/**
 * Faz a sincroniza��o no sentido servidor->cliente
 * !!! deve ser executado ap�s sync_client_to_server !!!
 *
 * Testa 4 condi��es:
 *
 *		(1) - FL e FR existem e s�o diferentes
 *				Modificado no server, faz download de FR e atualiza FS
 *		(2) - FL e FR existem e s�o iguais
 *				Igual, faz nada
 *		(3)	- FL existe e FR n�o
 *				FR foi apagado do server, apaga FL e FS
 *		(4) - FR existe e FL n�o
 *				Novo arquivo no cliente, upload de FL
 * Obs:
 *	FL	-> Arquivo local no sync dir do usu�rio
 *	FR	-> Arquivo remoto no servidor
 *	FS	-> Arquivo de controle do arquivo correspondente no sync dir
 *
 **/
void sync_server_to_client()
{
	char * client_files, * server_files;
	char ** client_files_arr, ** server_files_arr;
	int client_size, server_size;

	int i, j, found;
	time_t local_mtime, remote_mtime;
	char path[BUFFER_SIZE], path_sys[BUFFER_SIZE];

	client_size = 0;
	server_size = 0;

	client_files = get_file_list(homedir);
	server_files = get_file_list_command();

	if (client_files)
	{
		explode(client_files, "\n", &client_files_arr, &client_size);
		free(client_files);
	}

	if (server_files)
	{
		explode(server_files, "\n", &server_files_arr, &server_size);
		free(server_files);
	}

	for (i = 0; i < client_size; i++)
	{
		found = 0;
		sprintf(path, "%s/%s", homedir, client_files_arr[i]);
		sprintf(path_sys, "%s/%s", systemdir, client_files_arr[i]);

		for (j = 0; j < server_size; j++)
		{
			if (strcmp(client_files_arr[i], server_files_arr[j]) == 0)
			{
				found = 1;
				break;
			}
		}

		if (found == 1)
		{
			local_mtime = get_mtime(path);
			remote_mtime = request_mtime_command(server_files_arr[j]);

			if (difftime(remote_mtime, local_mtime) != 0)
			{
				download_command(path);
				set_mtime(path_sys, remote_mtime);
				//set_mtime(path, remote_mtime);
			}
		}
		else
		{
			delete_file(path);
			delete_file(path_sys);
		}
	}

	for (i = 0; i < server_size; i++)
	{
		found = 0;
		for (j = 0; j < client_size; j++)
		{
			if (strcmp(server_files_arr[i], client_files_arr[j]) == 0)
			{
				found = 1;
				break;
			}
		}

		if (found == 0)
		{
			sprintf(path, "%s/%s", homedir, server_files_arr[i]);
			sprintf(path_sys, "%s/%s", systemdir, server_files_arr[i]);

			remote_mtime = request_mtime_command(server_files_arr[i]);
			download_command(path);

			write_file(path_sys, "", 0);
			set_mtime(path_sys, remote_mtime);
			//set_mtime(path, remote_mtime);
		}
	}

	if (server_size > 0)
	{
		for (i = 0; i < server_size; i++)
			free(server_files_arr[i]);
		free(server_files_arr);
	}

	if (client_size > 0)
	{
		for (i = 0; i < client_size; i++)
			free(client_files_arr[i]);
		free(client_files_arr);
	}
}


/**
 *
 **/
void sync_dir()
{
	sync_client_to_server();
	sync_server_to_client();
}


/**
 *
 **/
void * sync_dir_daemon(void * arg)
{
	while(1)
	{
		LOCK
		sync_dir();
		UNLOCK
		sleep(SYNC_TIME_INTERVAL);
	}

	return 0;
}

/**
 * HEARTBEAT
 * */
int heartbeat_client_init(char * host, int port)
{
	struct hostent *server;
    ///char ip[16];
    
	server = gethostbyname(host);

	if (server == NULL)
	{
		 printf("No such host\n");
	}
	else
	{
		if ((heart_fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
		{
			printf("Error opening socket\n");
            return 0;
		}
		else
		{
            timeout.tv_sec = 1;
            timeout.tv_usec = 0;
            
            FD_ZERO(&masterfds);
            FD_SET(heart_fd, &masterfds);
            
            memcpy(&readfds, &masterfds, sizeof(fd_set));
            
            if (select(heart_fd+1, &readfds, NULL, NULL, &timeout) < 0)
            {
                perror("on select");
                exit(1);
            }

            port++;
			serv_addr_heartbeat.sin_family = AF_INET;
			serv_addr_heartbeat.sin_port = htons(port);
			serv_addr_heartbeat.sin_addr = *((struct in_addr *)server->h_addr);
			bzero(&(serv_addr_heartbeat.sin_zero), 8); /// precisa?
            
            /*serv_addr.sin_family = AF_INET;
            serv_addr.sin_port = htons(PORT);
            serv_addr.sin_addr = *((struct in_addr *)server->h_addr);
            bzero(&(serv_addr.sin_zero), 8);*/
            return 1;
		}
	}

	return 0;
}

/**
 * Heartbeat Cliente
 **/
void * heartbeat_client(void * arg)
{
    char buffer[BUFFER_SIZE] = "";
    int peerlen = 100;
    
	while(isalive == 1)
    {
        strcpy(buffer,"alive");
        sendto(heart_fd, buffer, sizeof(buffer), 0, (struct sockaddr *)&serv_addr_heartbeat, peerlen);
        ///printf("Enviado 'alive'\n");
        
        if (FD_ISSET(heart_fd, &readfds))
        {
            if(recvfrom(heart_fd,buffer,sizeof(buffer),0,(struct sockaddr *) &serv_addr_heartbeat,(socklen_t *) &peerlen) < 0)
            {
                printf("ERROR: Couldn't receive from source!\n");
                ///close(heart_fd);
                count_alive++;
            }
        
            printf("Recebido %s\n\n",buffer);
        }
        
        if (count_alive == 5)
        {
            isalive = 0;
        }
        //else
        //{
            ///printf("\n ====== O servidor morreu! ====== \n");
            ///return 0;
        //}
        ///sleep(3);   /// depois mudamos para outro valor, s� botei este para testar mais r�pido
        //printf("ISALIVE: %d", isalive); 
    }
    
    printf("ISALIVE: %d", isalive); 
    
    printf("\n ====== O servidor morreu! ====== \n");
    close(heart_fd);
    return 0;
	///exit(-1);//return 0;
}

/**
 * Fun��o 'main()' do Cliente
 **/
int main(int argc, char **argv)
{
	pthread_t sync_th, front_th, heart_th;

	if (signal(SIGINT, sig_handler) == SIG_ERR)
		printf("Can't catch SIGINT\n");

	if (argc != 4)
	{
		printf("Usage: %s <username> <server IP> <server port>\n", argv[0]);
	}
	else
	{
		strcpy(userid, argv[1]);
		sprintf(homedir, "%s/sync_dir_%s", get_home(), userid);
		sprintf(systemdir, "%s/sync_dir_%s/.system", get_home(), userid);
        
        /// New (Heartbeat)
        if (heartbeat_client_init(argv[2], atoi(argv[3])) == 1)
        {
            printf("[DEBUG] Heartbeat Thread creation\n");
            pthread_create(&heart_th, NULL, heartbeat_client, NULL);
        }
        /// End New (Heartbeat)
        
		strcpy(server_ip, argv[2]);
		server_port = atoi(argv[3]);

		pthread_create(&front_th, NULL, frontend_daemon, NULL);

		while(frontend_ready == 0)
		{
			continue;
		}

		if (connect_server(FRONTEND_IP, FRONTEND_PORT))
		{
			init_dir();
			pthread_create(&sync_th, NULL, sync_dir_daemon, NULL);
			client_commands();

			LOCK
			exit_command();
			CloseSSL(client_ssl);
			close(client_fd);
			UNLOCK
		}
		else
		{
			printf("Failed to connect\n");
		}
	}

	return 0;
}
