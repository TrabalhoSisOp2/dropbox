#ifndef _DROPBOXUTIL_H_
#define _DROPBOXUTIL_H_

#include <arpa/inet.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <time.h>
#include <ucontext.h>
#include <unistd.h>
#include <utime.h>

/// Constantes do estado geral do programa
#define SUCCESS 1		/// função terminou com sucesso
#define ERROR 0			/// função executou incorretamente
#define FOUND 1			/// função encontrou um elemento
#define NOT_FOUND -1	/// função não encontrou um elemento
#define FAILURE -1		/// o sistema entrou em "pane", em uma situação fatal
#define INVALID -1		/// informação inválida
#define EQUAL 0			/// se os objetos comparados forem iguais

/// Constantes do programa
#define BUFFER_SIZE 256
#define TIME_SIZE 32

#define DEFAULT_TIME 0	/// O nome poderia ser também DEFAULT_MODIFICATION_TIME, sei lá, mas decidi deixar assim para ser mais genérico

/// Constantes das estruturas
#define MAXDEVICES 2	/// quant. máx. de dispositivos que um cliente pode conectar ao servidor
#define MAXNAME 65		/// incluindo o '\0'
#define MAXFILES 32	 	/// quant. máx. de arquivos por cliente no servidor
#define MAXCLIENTS 32	/// quant. máx. de clientes conectados ao servidor (?)
#define MAXPATH 255	 	/// Limite de tamanho de um caminho + nome de arquivo

#define DIR_ALREADY_EXISTS 1
#define DIR_JUST_CREATED 2
#define DIR_FAILED 0

#define SYNC_TIME_INTERVAL 5	 /// quantos segundos será o intervalo de sincronização do daemon

/// Constantes para interação com usuário
#define MENU_OPTIONS_AMOUNT 10

#define MENU_OPTION_UPLOAD 0
#define MENU_OPTION_DOWNLOAD 1
#define MENU_OPTION_LISTSERVER 2
#define MENU_OPTION_LISTCLIENT 3
#define MENU_OPTION_GETSYNCDIR 4
#define MENU_OPTION_GETFILELIST 5
#define MENU_OPTION_EXIT 6
#define MENU_OPTION_REMOVE_SERVER 7
#define MENU_OPTION_GET_MTIME 8
#define MENU_OPTION_CHANGE_MTIME 9
#define MENU_OPTION_INVALID -1

/***********************************************************************
 * name[MAXNAME] refere-se ao nome do arquivo.
 * extension[MAXNAME] refere-se ao tipo de extensão do arquivo.
 * last_modified [MAXNAME] refere-se a data da última modificação no arquivo.
 * size indica o tamanho do arquivo, em bytes.
 * ********************************************************************/
struct file_info {
	char name[MAXNAME];
	char extension[MAXNAME];
	char last_modified[MAXNAME];
	int size;
};

/***********************************************************************
 * devices[2] – associado aos dispositivos do usuário.
 * userid[MAXNAME] – id do usuário no servidor, que deverá ser único.
 *					 Informado pela linha de comando.
 * file_info[MAXFILES] – metadados de cada arquivo que o cliente possui no servidor.
 * logged_in – cliente está logado ou não.
 * ********************************************************************/
struct client {
	int devices[MAXDEVICES];
	char userid[MAXNAME];
	struct file_info file_info[MAXFILES];
	int logged_in;
};


int open_directory(char * dirname);
char * get_file_list(char * path);
char * list_directory(char * path);
char * list_file(char * path);
int send_data(SSL * ssl, char * data, int size);
char * read_data(SSL * ssl, int * size);
char * read_file(char * path, int * size);
void write_file(char * path, char * data, int size);
char * get_home();
time_t get_mtime(char * path);
int set_mtime(char * path, time_t t);
int delete_file(char *file);
int file_exist(char *filename);
int substr_count(char * search, char * string);
void explode(const char *src, const char *tokens, char ***list, int *len);

#endif
