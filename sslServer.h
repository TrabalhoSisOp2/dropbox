#ifndef _SSLSERVER_H_
#define _SSLSERVER_H_

#include "dropboxUtil.h"

#define CERTFILE "CertFile.pem"
#define KEYFILE "KeyFile.pem"

SSL_CTX* InitServerCTX(void);
void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile);
#endif