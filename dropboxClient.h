#ifndef _DROPBOXCLIENT_H_
#define _DROPBOXCLIENT_H_

#include "dropboxUtil.h"

extern SSL * client_ssl;
extern int client_fd;
extern char userid[MAXNAME];
extern char homedir[BUFFER_SIZE];
extern char systemdir[BUFFER_SIZE];
extern char server_ip[BUFFER_SIZE];
extern int server_port;
extern pthread_mutex_t mutex;

#define LOCK pthread_mutex_lock(&mutex);
#define UNLOCK pthread_mutex_unlock(&mutex);

int connect_server(char * host, int port);
void option_selector(int optioncode, char* token);
void client_commands();
int upload_command(char * path);
int download_command(char * filename);
int list_server_command();
int list_client_command(char * path);
char * get_file_list_command();
int remove_command(char * filename);
time_t request_mtime_command(char * filename);
int change_mtime_command(char * filename, time_t t);
void help_command();
int exit_command();
void get_sync_dir_command();
void close_connection();
void sig_handler(int signo);
void init_dir();
void sync_client_to_server();
void sync_server_to_client();
void sync_dir();
void * sync_dir_daemon(void * arg);
#endif
