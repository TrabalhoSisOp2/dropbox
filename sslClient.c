#include "sslClient.h"

SSL_CTX* InitCTX(void)
{
	const SSL_METHOD *method;
	SSL_CTX *ctx;

	OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
	SSL_load_error_strings();   /* load all error messages */
	method = SSLv23_client_method();  /* create new server-method instance */

	ctx = SSL_CTX_new(method);   /* create new context from method */

	if ( ctx == NULL )
	{
		ERR_print_errors_fp(stderr);
		abort();
	}

	return ctx;
}