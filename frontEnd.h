#ifndef _FRONTEND_H_
#define _FRONTEND_H_

#define FRONTEND_IP "127.0.0.1"
#define FRONTEND_PORT frontend_port

extern int frontend_ready;
extern int frontend_port;

void frontend_init(int port);
void frontend_listen(int port);
int frontend_connect_server(char * host, int port);
void * frontend_daemon(void * arg);

#endif