#include <time.h>
#include <stdlib.h>
#include "frontEnd.h"
#include "dropboxUtil.h"
#include "dropboxClient.h"
#include "sslServer.h"
#include "sslClient.h"

SSL * server_ssl;
int frontend_fd;
int frontend_ready = 0;
int frontend_port;

/**
 *
 **/
void frontend_init(int port)
{
	struct sockaddr_in serv_addr;

	if ((frontend_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		printf("frontend_init: Error opening socket\n");
		exit(1);
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(serv_addr.sin_zero), 8);

	if (bind(frontend_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		printf("frontend_init: Error on binding\n");
		exit(1);
	}
}


/**
 *
 **/
void frontend_listen(int port)
{
	socklen_t clilen;
	struct sockaddr_in cli_addr;
	int newclient_fd, size;
	char * buf;

	SSL * newclient_ssl;
	SSL_CTX *ctx;

	SSL_library_init();
	ctx = InitServerCTX();
	LoadCertificates(ctx, CERTFILE, KEYFILE);

	clilen = sizeof(struct sockaddr_in);
	listen(frontend_fd, 5);

	printf("frontend_listen: Listening on port %d\n", port);
	frontend_ready = 1;

	while(1)
	{
		if ((newclient_fd = accept(frontend_fd, (struct sockaddr *) &cli_addr, &clilen)) == -1)
		{
			printf("frontend_listen: Error on accept new connection\n");
		}
		else
		{
			newclient_ssl = SSL_new(ctx);
			SSL_set_fd(newclient_ssl, newclient_fd);

			if (SSL_accept(newclient_ssl) == -1)
			{
		        	ERR_print_errors_fp(stderr);
		        	return;
			}

			while(1)
			{
				buf = read_data(newclient_ssl, &size);

				if (buf)
				{
					if (send_data(server_ssl, buf, size))
					{
						free(buf);
						buf = read_data(server_ssl, &size);

						if (buf)
						{
							if (send_data(newclient_ssl, buf, size))
							{

							}
							else
							{
								printf("frontend_listen: Error sending reply to the client\n");
							}

							free(buf);
						}
					}
					else
					{
						free(buf);
						printf("frontend_listen: Error sending request to the server\n");
					}
				}
			}
		}
	}
}


/**
 *
 **/
int frontend_connect_server(char * host, int port)
{
	struct sockaddr_in serv_addr;
	struct hostent *server;
	int fd;

	SSL_CTX *ctx;

	SSL_library_init();
	ctx = InitCTX();

	server = gethostbyname(host);

	if (server == NULL)
	{
		 printf("frontend_connect_server: No such host\n");
	}
	else
	{
		if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		{
			printf("frontend_connect_server: Error opening socket\n");
		}
		else
		{
			serv_addr.sin_family = AF_INET;
			serv_addr.sin_port = htons(port);
			serv_addr.sin_addr = *((struct in_addr *)server->h_addr);
			bzero(&(serv_addr.sin_zero), 8);

			if (connect(fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
			{
				printf("frontend_connect_server: Error connecting\n");
			}
			else
			{
				server_ssl = SSL_new(ctx);
				SSL_set_fd(server_ssl, fd);

				if (SSL_connect(server_ssl) == -1)
				{
					ERR_print_errors_fp(stderr);
					exit(1);
				}

				return 1;
			}
		}
	}

	return 0;
}


/**
 *
 **/
void * frontend_daemon(void * arg)
{
	srand(time(NULL));
	frontend_port = 10000 + (rand() % 10000);

	frontend_init(frontend_port);

	if (frontend_connect_server(server_ip, server_port))
	{
		frontend_listen(frontend_port);
	}

	return 0;
}
