#include "dropboxUtil.h"
#include "dropboxServer.h"
#include "sslCommon.h"
#include "sslServer.h"

int server_fd;
LISTA * Clients;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/// New
int heart_fd;
struct sockaddr_in serv_addr_heartbeat;
/// End New (Heartbeat)

/// New Master
char iplist[4][32];  /// Até 4 réplicas
char replica_names[4][32] = {"Replica 1",   /// Precisaria disto?
                             "Replica 2",
                             "Replica 3",
                             "Replica 4"};
LISTA * Replicas;
int is_replica = 0;
int idx_replicas = 0;
/// End New Master

/**
 * Captura de sinais
 **/
void sig_handler(int signo)
{
	if (signo == SIGINT)
	{
		printf("received SIGINT\n");
		close_server();
		exit(0);
	}
}

/**
 * userid - userid do cliente
 * Pesquisa por dado usuário na lista de usuários
 * Retorna NULL caso não encontre
 **/
struct client * search_client(char * userid)
{
	int i;
	struct client * cli;

	for (i = 0; i < LSIZE(Clients); i++)
	{
		cli = LGET(Clients, i);
		if (cli)
		{
			if (strcmp(cli->userid, userid) == 0)
				return cli;
		}
	}

	return NULL;
}

/**
 * device - dispositivo do cliente. Atualmente eh o fd
 * Pesquisa por dado device na lista de usuários
 * Retorna NULL caso não encontre
 **/
struct client * search_device(int device)
{
	int i, j;
	struct client * cli;

	for (i = 0; i < LSIZE(Clients); i++)
	{
		cli = LGET(Clients, i);
		if (cli)
		{
			for (j = 0; j < MAXDEVICES; j++)
			{
				if (cli->devices[j] == 0)
					continue;

				if (cli->devices[j] == device)
					return cli;
			}
		}
	}

	return NULL;
}

/**
 * fd - id do socket que identifica o dispositivo conectado
 * Faz o fechamento do dispositivo do cliente identificado por fd
 * Caso o cliente não tenha outros dispositivos conectados,
 * o cliente também será fechado
 **/
void close_client_device(int fd)
{
	int i, devices;
	struct client * cli, * tmp;

	devices = 0;
	cli = search_device(fd);

	if (cli)
	{
		for (i = 0; i < MAXDEVICES; i++)
		{
			if (cli->devices[i] == fd)
				cli->devices[i] = 0;

			if (cli->devices[i] > 0)
				devices ++;
		}

		if (devices == 0)
		{
			for (i = 0; i < LSIZE(Clients); i++)
			{
				tmp = LGET(Clients, i);
				if (tmp)
				{
					if (strcmp(tmp->userid, cli->userid) == 0)
					{
						free(tmp);
						LDEL(Clients, i);
					}
				}
			}
		}
	}
}

/**
 * Inicia o servidor
 **/
void init_server(int port)
{
	struct sockaddr_in serv_addr;

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		printf("Error opening socket\n");
		exit(1);
	}

	struct stat st = {0};

	if (stat(SERVER_PATH, &st) == -1)
	{
		mkdir(SERVER_PATH, 0700);
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(serv_addr.sin_zero), 8);

	if (bind(server_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
	{
		printf("Error on binding\n");
		exit(1);
	}

	Clients = calloc(1, sizeof(LISTA));
	LINIT(Clients);
}

/**
 * Faz o servidor ouvir e o mesmo fica bloqueado aguardando Conexões
 **/
void listen_server(int port)
{
	socklen_t clilen;
	struct sockaddr_in cli_addr;
	struct client * cli;
	int newfd, size;
	char * buf, output[BUFFER_SIZE];

	SSL * newssl;
	SSL_CTX *ctx;

	SSL_library_init();
	ctx = InitServerCTX();
	LoadCertificates(ctx, CERTFILE, KEYFILE);

	clilen = sizeof(struct sockaddr_in);
	listen(server_fd, 5);

	printf("Server listening on port %d\n", port);

	while(1)
	{
		if ((newfd = accept(server_fd, (struct sockaddr *) &cli_addr, &clilen)) == -1)
		{
			printf("Error on accept new connection\n");
		}
		else
		{
			newssl = SSL_new(ctx);
			SSL_set_fd(newssl, newfd);

			if (SSL_accept(newssl) == -1)
			{
		        	ERR_print_errors_fp(stderr);
		        	return;
			}

		 	buf = read_data(newssl, &size);

			if (buf == NULL)
			{
				printf("Error reading userid from socket\n");
			}
			else
			{
				LOCK
				cli = search_client(buf);

				if (cli == NULL)
				{
					cli = calloc(1, sizeof(struct client));
					cli->devices[0] = SSL_get_fd(newssl);
					cli->logged_in = 1;
                    
                    /** Será que é assim? Tem que diferenciar as réplicas dos clientes (já que ambos serão na visão do servidor mestre?)
                     * Como mudar para aceitar conexões de réplicas? Hmmm...
                    if (is_replica == 0)
                    {
                        strcpy(cli->userid, buf);
                        
                        printf("New client %s\n", cli->userid);

                        LAPPEND(Clients, cli);
                    }
                    else
                    {
                        strcpy(cli->userid, replica_names[idx_replicas]);
                        
                        printf("New replica %s\n", cli->userid);

                        LAPPEND(Replica, cli);
                    }
                    **/
                    
					/* At this point, the client is allowed to connect,
					 we should make a new thread to process commands */
					{
						pthread_t th;
						pthread_create(&th, NULL, server_command, newssl);
					}
				}
				else
				{
					int i, found = 0;

					for (i = 0; i < MAXDEVICES; i++)
					{
						if (cli->devices[i] == 0)
						{
							cli->devices[i] = SSL_get_fd(newssl);
							cli->logged_in = 1;
							found = 1;
							break;
						}
					}

					if (found == 0)
					{
						sprintf(output, "No device slot available");
						send_data(newssl, output, strlen(output) + 1);
						CloseSSL(newssl);
					}
					else
					{
						printf("Existing client %s\n", cli->userid);
                        
                        /// Seria aqui?
                        /// connect_server_replicas(port);
                        
						/* At this point, the client is allowed to connect,
						 we should make a new thread to process commands */
						{
							pthread_t th;
							pthread_create(&th, NULL, server_command, newssl);
						}
					}
				}
				UNLOCK
			}
		}
	}
}

/**
 * Processa os comandos enviados pelo cliente
 * Esta função sempre vai rodar numa nova thread
 * arg - ponteiro para o ssl do cliente
 **/
void * server_command(void * arg)
{
	int fd, size, ok;
	struct client * cli;
	char * buffer, * token;
	char output[BUFFER_SIZE], homedir[BUFFER_SIZE];
	SSL * ssl;

	if (arg)
	{
		ssl = (SSL *) arg;
		fd = SSL_get_fd(ssl);
		cli = search_device(fd);

		if (cli)
		{
			sprintf(homedir, "%s/sync_dir_%s", SERVER_PATH, cli->userid);

			if ((ok = open_directory(homedir)))
			{
				sprintf(output, "Welcome %s", cli->userid);
			}
			else
			{
				sprintf(output, "Failed to open homedir");
			}

			send_data(ssl, output, strlen(output) + 1);

			while(ok)
			{
				buffer = read_data(ssl, &size);

				if (buffer)
				{
					printf("User %s on device %d sent %s\n", cli->userid, fd, buffer);

					token = strtok(buffer, " ");

					/* Upload command */
					if (strcmp(token, "upload") == 0)
					{
						char path[BUFFER_SIZE];

						token = strtok(NULL, "\n");

						if (token)
						{
							token = basename(token);
							sprintf(path, "%s/%s", homedir, token);
							upload_command(ssl, path);
						}
					}

					/* Download command */
					else if (strcmp(token, "download") == 0)
					{
						char path[BUFFER_SIZE];

						token = strtok(NULL, "\n");

						if (token)
						{
							token = basename(token);
							sprintf(path, "%s/%s", homedir, token);
							download_command(ssl, path);
						}
					}

					/* List server command */
					else if (strcmp(token, "list_server") == 0)
					{
						list_server_command(ssl, homedir);
					}

					/* Get file list command */
					else if (strcmp(token, "get_file_list") == 0)
					{
						get_file_list_command(ssl, homedir);
					}

					/* Exit command */
					else if (strcmp(token, "exit") == 0)
					{
						exit_command(ssl);
						break;
					}

					/* Modification time request command */
					else if (strcmp(token, "request_mtime") == 0)
					{
						char path[BUFFER_SIZE];

						token = strtok(NULL, "\n");

						if (token)
						{
							token = basename(token);
							sprintf(path, "%s/%s", homedir, token);
							request_mtime_command(ssl, path);
						}
					}

					/* Modification time change command */
					else if (strcmp(token, "change_mtime") == 0)
					{
						char path[BUFFER_SIZE];
						time_t t;

						token = strtok(NULL, " ");

						if (token)
						{
							t = atoi(token);
							token = strtok(NULL, "\n");

							if (token)
							{
								token = basename(token);
								sprintf(path, "%s/%s", homedir, token);
								change_mtime_command(ssl, path, t);
							}
						}
					}

					/* Delete file request */
					else if (strcmp(token, "delete_file") == 0)
					{
						char path[BUFFER_SIZE];

						token = strtok(NULL, "\n");

						if (token)
						{
							token = basename(token);
							sprintf(path, "%s/%s", homedir, token);
							delete_file_command(ssl, path);
						}
					}

					free(buffer);
				}
				else
				{
					printf("Erro reading command from %s on device %d\n", cli->userid, fd);
				}
			}
		}
		else
		{
			sprintf(output, "Client not initialized");
			send_data(ssl, output, strlen(output) + 1);
		}

		LOCK
		close_client_device(fd);
		CloseSSL(ssl);
		close(fd);
		UNLOCK
	}

	return 0;
}

/**
 *
 **/
int upload_command(SSL * ssl, char * filename)
{
	int size;
	char * buffer, output[BUFFER_SIZE];

	buffer = read_data(ssl, &size);

	if (buffer)
	{
		write_file(filename, buffer, size);
		free(buffer);

		sprintf(output, "success");
		send_data(ssl, output, strlen(output) + 1);

		return 1;
	}
	else
	{
		sprintf(output, "error");
		send_data(ssl, output, strlen(output) + 1);

		return 0;
	}
}

/**
 *
 **/
int download_command(SSL * ssl, char * filename)
{
	int size, result;
	char * buffer, output[BUFFER_SIZE];

	buffer = read_file(filename, &size);

	if (buffer)
	{
		result = send_data(ssl, buffer, size);
		free(buffer);

		return result;
	}
	else
	{
		sprintf(output, "error");
		send_data(ssl, output, strlen(output) + 1);

		return 0;
	}
}

/**
 *
 **/
int list_server_command(SSL * ssl, char * homedir)
{
	int result;
	char * buffer, output[BUFFER_SIZE];

	buffer = list_directory(homedir);

	if (buffer)
	{
		result = send_data(ssl, buffer, strlen(buffer) + 1);
		free(buffer);

		return result;
	}
	else
	{
		sprintf(output, "error");
		send_data(ssl, output, strlen(output) + 1);

		return 0;
	}
}

/**
 *
 **/
int get_file_list_command(SSL * ssl, char * homedir)
{
	int result;
	char * buffer, output[BUFFER_SIZE];

	buffer = get_file_list(homedir);

	if (buffer)
	{
		result = send_data(ssl, buffer, strlen(buffer) + 1);
		free(buffer);

		return result;
	}
	else
	{
		sprintf(output, "error");
		send_data(ssl, output, strlen(output) + 1);

		return 0;
	}
}

/**
 *
 **/
int request_mtime_command(SSL * ssl, char * filename)
{
	time_t t;
	int result;
	char output[BUFFER_SIZE];

	t = get_mtime(filename);

	if (t)
	{
		sprintf(output, "%-255ld", t);
		result = send_data(ssl, output, strlen(output) + 1);

		return result;
	}
	else
	{
		sprintf(output, "error");
		send_data(ssl, output, strlen(output) + 1);

		return 0;
	}
}

/**
 *
 **/
int change_mtime_command(SSL * ssl, char * filename, time_t t)
{
	int result;
	char output[BUFFER_SIZE];

	result = set_mtime(filename, t);

	if (result)
	{
		sprintf(output, "success");
		send_data(ssl, output, strlen(output) + 1);

		return 1;
	}
	else
	{
		sprintf(output, "error");
		send_data(ssl, output, strlen(output) + 1);

		return 0;
	}
}

/**
 *
 **/
int delete_file_command(SSL * ssl, char * filename)
{
	int result;
	char output[BUFFER_SIZE];

	result = delete_file(filename);

	if (result)
	{
		sprintf(output, "success");
		send_data(ssl, output, strlen(output) + 1);

		return 1;
	}
	else
	{
		sprintf(output, "error");
		send_data(ssl, output, strlen(output) + 1);

		return 0;
	}
}

/**
 *
 **/
int exit_command(SSL * ssl)
{
	int result;
	char output[BUFFER_SIZE];

	sprintf(output, "success");
	result = send_data(ssl, output, strlen(output) + 1);

	return result;
}

/**
 * Faz o fechamento do server e todos os clientes conectados
 **/
void close_server()
{
	int i, j;
	struct client * cli;

	for (i = 0; i < LSIZE(Clients); i++)
	{
		cli = LGET(Clients, i);
		if (cli)
		{
			for (j = 0; j < MAXDEVICES; j++)
			{
				if (cli->devices[j] != 0)
				{
					close(cli->devices[j]);
				}
			}
			free(cli);
		}
		LDEL(Clients, i);
	}

	if (server_fd)
	{
		close(server_fd);
		server_fd = 0;
	}
}

/**
 * HEARTBEAT
 * */
int heartbeat_server_init(int port)
{
    int peerlen = 100;
    
    if ((heart_fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        printf("Error opening UDP socket\n");
        return -1;
    }
    
    port++;
    memset((void *)&serv_addr_heartbeat,0,sizeof(struct sockaddr_in));
    serv_addr_heartbeat.sin_family = AF_INET;
    serv_addr_heartbeat.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr_heartbeat.sin_port = htons(port);
    peerlen = sizeof(serv_addr_heartbeat);
    
    if(bind(heart_fd,(struct sockaddr *) &serv_addr_heartbeat, peerlen) == -1)
    {
        printf("ERROR: Couldn't bind to the port given!\n");
        close(heart_fd);
        return -1;
    }

    printf("Socket initialized successfully! Awaiting messages...\n\n");
    
	return 1;
}

/**
 * Heartbeat Server
 **/
void * heartbeat_server(void * arg)
{
    char * buffer = "";
    int peerlen = 100;
    
	while(1)
    {
        if (recvfrom(heart_fd,buffer,sizeof(buffer),0,(struct sockaddr *) &serv_addr_heartbeat,(socklen_t *) &peerlen) < 0)
        {
            printf("[HEARTBEAT SERVER] Error on revfrom()!\n");
        }
        
        if (sendto(heart_fd, "i'm alive!\n", sizeof(buffer), 0, (struct sockaddr *)&serv_addr_heartbeat, peerlen) < 0)
        {
            printf("[HEARTBEAT SERVER] Error on sendto()!\n");
        }
    }
	return 0;
}

/**
 * OBS: COPIADO DE DROPBOXCLIENT.C
 * 
 * Conecta o servidor réplica com o servidor mestre.
 * host - endereo do servidor
 * port - port aguardando conexo
 * Retorna o fd da conexo
 **/
/*
int connect_server(char * host, int port)
{
	int size;
	char * buffer;
	struct sockaddr_in serv_addr;
	struct hostent *server;

	SSL_CTX *ctx;

	SSL_library_init();
	ctx = InitCTX();

	server = gethostbyname(host);

	if (server == NULL)
	{
		 printf("No such host\n");
	}
	else
	{
		if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		{
			printf("Error opening socket\n");
		}
		else
		{
			serv_addr.sin_family = AF_INET;
			serv_addr.sin_port = htons(port);
			serv_addr.sin_addr = *((struct in_addr *)server->h_addr);
			bzero(&(serv_addr.sin_zero), 8);

			if (connect(client_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
			{
				printf("Error connecting\n");
			}
			else
			{

				client_ssl = SSL_new(ctx);
				SSL_set_fd(client_ssl, client_fd);

				if (SSL_connect(client_ssl) == -1)
				{
					ERR_print_errors_fp(stderr);
					exit(1);
				}

				if (send_data(client_ssl, userid, strlen(userid) + 1))
				{
					buffer = read_data(client_ssl, &size);

					if (buffer)
					{
						printf("%s\n", buffer);

						if (strstr(buffer, "Welcome"))
						{
							free(buffer);

							return 1;
						}

						free(buffer);
					}
					else
					{
						printf("Error getting response\n");
					}
				}
				else
				{
					printf("Error sending data\n");
				}
			}
		}
	}

	return 0;
}*/

/**
 * Função 'main()' do Servidor
 **/
int main(int argc, char *argv[])
{
    pthread_t heart_th;
	int port;

	if (signal(SIGINT, sig_handler) == SIG_ERR)
	{
		printf("Can't catch SIGINT\n");
	}

	if (argc < 3)
	{
		printf("Usage: %s <server port> <master|replica> <ip list>\n", argv[0]);
		return 1;
	}

	port = atoi(argv[1]);
    
    /// New (Heartbeat)
    if (heartbeat_server_init(atoi(argv[1])))
    {
        printf("[DEBUG] Heartbeat Thread creation");
        pthread_create(&heart_th, NULL, heartbeat_server, NULL);
    }
    /// End New (Heartbeat)
    
    int i, j;
    j = 0;
    for (i = 3; i < argc; i++)
    {
        strcpy(iplist[i], argv[i]);
    }
    
    //iplist[0] = argv[3];
    
    /*for (i = 3; i < argc; i++)
    {
        puts(iplist[i]);
    }*/
    
    if (strcmp("master", argv[2]) == 0)
    {
        /// DEBUG
        printf("\nMASTER!\n\n");
        
        init_server(port);
        listen_server(port);
        close_server();        
    }
    else if (strcmp("replica", argv[2]) == 0)
    {
        /// DEBUG
        printf("\nREPLICA!\n\n");
        
        is_replica = 1;
        init_server(port);
        ///connect_to_master(iplist[0], port);
        //listen_server(port);
        close_server();
    }
    else
    {
        printf("ERROR: Invalid arg %s\n\n", argv[2]);
        return -1;
    }
    
	/*init_server(port);
	listen_server(port);
	close_server();*/

	return 0;
}
