#ifndef _DROPBOXSERVER_H_
#define _DROPBOXSERVER_H_

#include "lista.h"

#define SERVER_PATH "DropboxOS"

#define LOCK pthread_mutex_lock(&mutex);
#define UNLOCK pthread_mutex_unlock(&mutex);

extern pthread_mutex_t mutex;

void sig_handler(int signo);
struct client * search_client(char * userid);
struct client * search_device(int device);
void close_client_device(int fd);
void init_server();
void listen_server();
void * server_command(void * arg);
int upload_command(SSL * ssl, char * filename);
int download_command(SSL * ssl, char * filename);
int list_server_command(SSL * ssl, char * homedir);
int get_file_list_command(SSL * ssl, char * homedir);
int request_mtime_command(SSL * ssl, char * filename);
int change_mtime_command(SSL * ssl, char * filename, time_t t);
int delete_file_command(SSL * ssl, char * filename);
int exit_command(SSL * ssl);
void close_server();
SSL_CTX* InitServerCTX(void);
void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile);
void ShowCerts(SSL* ssl);

#endif