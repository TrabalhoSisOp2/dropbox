/**
 * Partes do código extraídos e adaptados de:
 *  http://www.binarytides.com/get-local-ip-c-linux/
 *  https://linux.die.net/man/3/qsort
 * */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <unistd.h>

char myIP[NI_MAXHOST];

void checkMyIP()
{
    FILE *f;
    char line[100] , *p , *c;
     
    f = fopen("/proc/net/route" , "r");
     
    while(fgets(line , 100 , f))
    {
        p = strtok(line , " \t");
        c = strtok(NULL , " \t");
         
        if(p!=NULL && c!=NULL)
        {
            if(strcmp(c , "00000000") == 0)
            {
                break;
            }
        }
    }
     
    //which family do we require , AF_INET or AF_INET6
    int fm = AF_INET;
    struct ifaddrs *ifaddr, *ifa;
    int family , s;
 
    if (getifaddrs(&ifaddr) == -1) 
    {
        perror("getifaddrs");
        exit(EXIT_FAILURE);
    }
 
    //Walk through linked list, maintaining head pointer so we can free list later
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
        {
            continue;
        }
 
        family = ifa->ifa_addr->sa_family;
 
        if(strcmp( ifa->ifa_name , p) == 0)
        {
            if (family == fm) 
            {
                s = getnameinfo( ifa->ifa_addr, (family == AF_INET) ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6) , myIP , NI_MAXHOST , NULL , 0 , NI_NUMERICHOST);
                 
                if (s != 0)
                {
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
    freeifaddrs(ifaddr);
}

/*
    The actual arguments to this function are "pointers to
    pointers to char", but strcmp(3) arguments are "pointers
    to char", hence the following cast plus dereference
*/
       
static int cmpstringp(const void *p1, const void *p2)
{
    return strcmp(* (char * const *) p1, * (char * const *) p2);
}

/**
 * Sorting
 **/
void sortIPs(int argc, char* argv[])
{
    qsort(&argv[1], argc - 1, sizeof(char *), cmpstringp);
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s <string>...\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    
    sortIPs(argc, argv);
    
    /// DEBUG
    ///int j;
    
    /**for (j = 1; j < argc; j++)
        puts(argv[j]);**/
    
    checkMyIP();
    
    printf("SELECTED IP: ");
    puts(argv[1]);
    
    printf("MY IP: ");
    puts(myIP);
    
    /// Needs fixing (i think i fixed)
    if (strcmp(myIP, argv[1]) == 0)
    {
        printf("I'M THE MASTER!\n");
        /// init_server_functions();
    }
    else
    {
        printf("I'M THE REPLICA!\n");
        /// init_server_functions();
        /// connect_to_master();
    }
    
    exit(EXIT_SUCCESS);
}
