#ifndef _SSLCOMMON_H_
#define _SSLCOMMON_H_

#include "dropboxUtil.h"

void ShowCerts(SSL* ssl);
void CloseSSL(SSL * ssl);
#endif