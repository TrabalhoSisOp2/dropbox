#include <stdio.h>
#include <stdlib.h>
#include "lista.h"


/**
 * Inicia uma Lista
 **/
void listaInicializar(LISTA * lista)
{
	*lista = NULL;
}


/**
 * Imprime os elementos da lista com cast para int
 **/
static void listaListarSub(LISTA *lista, int i)
{
	int *p;

 	if (*lista)
 	{
		p = (*lista)->dado;
 		printf("Dado (%d): %d\n", i, *p);
		listaListarSub(&(*lista)->prox, i + 1);
	}
}

void listaListar(LISTA * lista)
{
	listaListarSub(lista, 0);
}


/**
 * Retorna o tamanho da lista
 **/
static int listaTamanhoSub(LISTA *lista, int i)
{
	if (*lista)
	{
		return listaTamanhoSub(&(*lista)->prox, i + 1);
	}
	else
	{
		return i;
	}
}

int listaTamanho(LISTA *lista)
{
	return listaTamanhoSub(lista, 0);
}


/**
 * Retorna um elemento da lista na posicao desejada
 **/
static void * listaPegarEmSub(LISTA * lista, int posicao, int i)
{
	if (*lista)
	{
		if (i == posicao)
		{
			return (*lista)->dado;
		}
		else
		{
			return listaPegarEmSub(&(*lista)->prox, posicao, i + 1);
		}
	}
	else
	{
		return NULL;
	}
}

void * listaPegarEm(LISTA * lista, int posicao)
{
	return listaPegarEmSub(lista, posicao, 0);
}


/**
 * Insere um elemento na lista na posicao desejada
 * Caso a posição seja maior que o tamanho ou não exista, vai inserir no fim
 **/
static void listaInserirEmSub(LISTA * lista, int posicao, void * dado, int i)
{
	NO * novoNo;

	if (*lista)
	{
		if (i == posicao)
		{
			novoNo = (LISTA) calloc(1, sizeof(NO));
			if (novoNo)
			{
				novoNo->dado = dado;
				novoNo->prox = *lista;
				*lista = novoNo;
			}
		}
		else
		{
			listaInserirEmSub(&(*lista)->prox, posicao, dado, i + 1);
		}
	}
 	else
 	{
  		*lista = (LISTA) calloc(1, sizeof(NO));
  		if (*lista)
  		{
 			(*lista)->prox = NULL;
			(*lista)->dado = dado;
  		}
 	}
}

void listaInserirEm(LISTA * lista, int posicao, void * dado)
{
	listaInserirEmSub(lista, posicao, dado, 0);
}


/**
 * Remove um elemento na lista na posicao desejada
 **/
static void listaRemoverEmSub(LISTA * lista, LISTA * prev,  int posicao, int i)
{
	NO * tmpNo;

	if (*lista)
	{
		if (posicao == i)
		{
			if (prev)
			{
				tmpNo = *lista;
				(*prev)->prox = tmpNo->prox;
				free(tmpNo);
			}
			else
			{
				tmpNo = *lista;
				*lista = tmpNo->prox;
				free(tmpNo);
			}
		}
		else
		{
			listaRemoverEmSub(&(*lista)->prox, lista, posicao, i + 1);
		}
	}
}

void listaRemoverEm(LISTA * lista, int posicao)
{
	listaRemoverEmSub(lista, NULL, posicao, 0);
}